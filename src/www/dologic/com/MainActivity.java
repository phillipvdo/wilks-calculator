package www.dologic.com;

import java.lang.Math;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private RadioGroup gender, units;
	private EditText bodyWeight, liftedWeight;
	private Button calculate;
	private int genderSelected, unitSelected;
	private double resultValue;
	private TextView result;
	private String bodyWeightSelected, liftedWeightSelected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Gets the radio group
		gender = (RadioGroup)findViewById(R.id.radioGroup1);

		//gender.setOnCheckedChangeListener((OnCheckedChangeListener) this);
		gender.setOnCheckedChangeListener(
				new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						// Gets the radio button in the radio group that is checked
						RadioButton checkedradioButton_gender = (RadioButton)gender.findViewById(gender.getCheckedRadioButtonId());

						Log.v("Selected", "New radio item selected: " + checkedId);
						genderSelected = onRadioButtonClicked(checkedradioButton_gender);
					}
				});

		units = (RadioGroup)findViewById(R.id.radioGroup2);

		units.setOnCheckedChangeListener(
				new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						RadioButton checkedradioButton_units = (RadioButton)units.findViewById(units.getCheckedRadioButtonId());

						Log.v("Selected", "New radio item selected: " + checkedId);
						unitSelected = onRadioButtonClicked(checkedradioButton_units);
					}
				});

		bodyWeight = (EditText)findViewById(R.id.editText_bodyWeight);
		liftedWeight = (EditText)findViewById(R.id.editText_liftedWeight);
		result = (TextView)findViewById(R.id.resultCalc);

		calculate = (Button)findViewById(R.id.button_calculate);
		calculate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				bodyWeightSelected = bodyWeight.getText().toString();
				liftedWeightSelected = liftedWeight.getText().toString();

				//if (gender == 0 || units == 0 || x == 0 || y == 0){
				if (genderSelected == 0 || unitSelected == 0){
					Toast.makeText(getApplicationContext(), "Please select the units and your gender.", Toast.LENGTH_SHORT).show();
				} else {
					try{
						resultValue = wilkCalculate(genderSelected, unitSelected, bodyWeightSelected, liftedWeightSelected);
						result.setText(Double.toString(resultValue));
					}catch(IllegalArgumentException e){
						Toast.makeText(getApplicationContext(), "Please enter a number for bodyweight and lifted weight.", Toast.LENGTH_SHORT).show();
						System.out.println("Error: " + e);
					//Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();
					//}catch(ArithmeticException e){
					//	Toast.makeText(getApplicationContext(), "Testddddd", Toast.LENGTH_SHORT).show();
					//}
						//Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();
						//throw new IllegalArgumentException();
					}
				}				
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// Determine which radio button of the radio group is selected
	public int onRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch(view.getId()) {
		case R.id.radio0_grp1:
			if (checked)
				// Selection: Male / lbs
				return 1;
			break;
		case R.id.radio0_grp2:
			if (checked)
				// Selection: Male / lbs
				return 1;
			break;
		case R.id.radio1_grp1:
			if (checked)
				// Selection: Female / kg
				return 2;
			break;
		case R.id.radio1_grp2:
			if (checked)
				// Selection: kg
				return 2;
			break;
		}
		return 0;
	}

	public double wilkCalculate(int gender, int units, String bodyWeight, String liftedWeight){

		double Coeff = 0, result = 0, x = 0, y = 0, a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;

		//try{
			x = Double.parseDouble(bodyWeight); 
			y = Double.parseDouble(liftedWeight);
		//}catch(NumberFormatException Ex){
		//	Toast.makeText(getApplicationContext(), "Please enter a number for bodyweight and lifted weight.", Toast.LENGTH_SHORT).show();
		//}

		//if (gender == 0 || units == 0){
		//	Toast.makeText(getApplicationContext(), "Please select the units and your gender.", Toast.LENGTH_SHORT).show();
		//	throw new IllegalArgumentException();
		//} else {
		if (gender == 1){
			a = -216.0475144; b = 16.2606339; c = -0.002388645; d = -0.00113732; e = 7.01863E-06; f = -1.291E-08;
		} else if (gender == 2){
			a = 594.31747775582; b = -27.23842536447; c = 0.82112226871; d = -0.00930733913; e = 0.00004731582; f = -0.00000009054;
		}

		if (units == 1){
			x = x*0.453592;
			y = y*0.453592;
		} else if(units == 2){
			x = x;
			y = y;
		}

		Coeff = 500/(a+b*x+c*Math.pow(x,2)+d*Math.pow(x,3)+e*Math.pow(x,4)+f*Math.pow(x,5));
		result = Coeff*y;
		//}

		return result;
	}
}

